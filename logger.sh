#!/bin/bash
virsh destroy fresh-fedora
sudo rm -rf /tmp/newlog
sudo virsh start fresh-fedora
PROMPT="localhost login:  "
for (( ; ; ))
do
    LINE=$(tail -n 1 /tmp/newlog)
    if [ "${PROMPT:0:16}" == "${LINE:0:16}" ]
	then
	exit 0
    fi
done
